import copy
import os
import pytest
import unittest
from unittest.mock import patch

from pop_config.config.contracts.order import (
    _insert_default_placeholders,
    _DefaultOption,
    _restore_raw_defaults,
    _get_root,
    _reroot_path,
    _reroot_paths,
)


class TestRootsExpansion(unittest.TestCase):
    raw = {
        "idem": {
            "CONFIG": {
                "cache_dir": {"default": "/var/cache/idem"},
                "root_dir": {"default": "/"},
            }
        }
    }

    def test_stash_defaults(self):
        expected_defaults = {"idem": {"cache_dir": "/var/cache/idem", "root_dir": "/"}}
        expected_raw = {
            "idem": {
                "CONFIG": {
                    "cache_dir": {"default": _DefaultOption},
                    "root_dir": {"default": _DefaultOption},
                }
            }
        }

        # assert raw == ret_to_raw(ret)
        # assert raw_default == ret_to_raw(ret, "DEFAULT")
        raw = copy.deepcopy(self.raw)
        defaults = _insert_default_placeholders(raw)
        self.assertEqual(defaults, expected_defaults)
        self.assertEqual(raw, expected_raw)

        _restore_raw_defaults(raw, defaults)
        self.assertEqual(raw, self.raw)

    def test_get_root(self):
        with patch(
            "pop_config.config.contracts.order.os.geteuid",
            autospec=True,
            return_value=0,
        ):
            # make sure we get None if root isn't an option:
            self.assertIsNone(_get_root({}, "cli"))

            root = _get_root({"cli": {"no_root"}}, "cli")
            self.assertIsNone(root, None)

            # make sure specified root is retained:
            ret = {"cli": {"root_dir": "myroot"}}
            root = _get_root(ret, "cli")
            self.assertEqual(root, "myroot")

            # make sure we don't create a user-root by default if running as root:
            ret = {"cli": {"root_dir": _DefaultOption}}
            root = _get_root(ret, "cli")
            self.assertEqual(root, None)

        with patch(
            "pop_config.config.contracts.order.os.geteuid",
            autospec=True,
            return_value=12345,
        ):
            # make sure we don't create a user-root by default if running as root:
            ret = {"cli": {"root_dir": _DefaultOption}}
            root = _get_root(ret, "cli")
            self.assertEqual(root, os.path.expanduser("~/.cli"))

    def test_reroot_path(self):
        data = [
            ("/ROOT", "/usr/var/log/MATCH", "/ROOT/usr/var/log/MATCH"),
            ("/ROOT", "/usr/var/log/MATCH/", "/ROOT/usr/var/log/MATCH/"),
            ("/ROOT", "/usr/var/log/MATCH/post", "/ROOT/usr/var/log/MATCH/post"),
            ("/ROOT", "/usr/var/log/MATCH/post/", "/ROOT/usr/var/log/MATCH/post/"),
            ("/ROOT", "/usr/var/log/MATCH", "/ROOT/usr/var/log/MATCH"),
            ("/ROOT", "relative/path", "relative/path"),
            ("/ROOT/", "/usr/var/log/MATCH", "/ROOT/usr/var/log/MATCH"),
            ("/MULTI/ROOT/", "/MATCH", "/MULTI/ROOT/MATCH"),
        ]
        for root, val, expected in data:
            ret = _reroot_path(val, imp="MATCH", new_root=root)
            self.assertEqual(ret, expected)

    def test_reroot_paths(self):
        ret = {
            "replace": {
                "match_abs_dir": "/var/log/replace",
                "match_abs_path": "/var/log/replace/",
                "match_abs_file": "/var/log/replace",
                "nomatch_abs_dir": "/path/to/not_replace",
                "nomatch_absloc_dir": "/path/to/replace_not/loc",
                "nomatch_relative_dir": "relative/replace/dir",
            }
        }
        _reroot_paths(ret, "new_root")
        for k, v in ret["replace"].items():
            if k.startswith("match_"):
                assert v.startswith("new_root"), k
            else:
                assert "new_root" not in v, k

    def test_reroot_paths_multiple_roots(self):
        defaults = {
            "main": {"root_dir": "/", "other_dir": "/a/main/dir"},
            "other": {"root_dir": "/", "other_dir": "/a/other/dir"},
        }
        expected = {
            "main": {"root_dir": "new_root", "other_dir": "new_root/a/main/dir"},
            "other": {"root_dir": "new_root", "other_dir": "new_root/a/other/dir"},
        }
        _reroot_paths(defaults, "new_root")
        self.assertEqual(expected, defaults)

    def test_reroot_paths_nochange(self):
        # re-rooting paths the default value ("/") should result in identical values.
        defaults = {"cli": {"root_dir": "/", "other_dir": "/an/other/dir"}}
        expected = copy.deepcopy(defaults)
        _reroot_paths(defaults, "/")
        self.assertEqual(expected, defaults)

    def test_no_default_is_error(self):
        raw = copy.deepcopy(self.raw)
        del raw["idem"]["CONFIG"]["cache_dir"]["default"]
        with pytest.raises(
            KeyError, match="No default value for 'cache_dir' in 'idem's conf.py"
        ):
            _insert_default_placeholders(raw)
